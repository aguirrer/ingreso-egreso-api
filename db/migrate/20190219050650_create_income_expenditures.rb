class CreateIncomeExpenditures < ActiveRecord::Migration[5.1]
  def change
    create_table :income_expenditure_types do |t|
      t.string :name
      t.text :description

      t.timestamps
    end

    create_table :income_expenditures do |t|
      t.string :uid
      t.belongs_to :user, index: true
      t.belongs_to :income_expenditure_type, index: true
      t.float :mount
      t.text :description

      t.timestamps
    end
  end
end
