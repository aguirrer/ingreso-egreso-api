Rails.application.routes.draw do
  # For details on the DSL available within this file, see http://guides.rubyonrails.org/routing.html
  namespace :api do
    namespace :v1 do
      post 'login', to: 'sessions#login'
      post 'signup', to: 'users#create'
      get 'users', to: 'users#show'
      resources :income_expenditures
    end
  end
end
