# == Schema Information
#
# Table name: income_expenditures
#
#  id                         :integer          not null, primary key
#  uid                        :string
#  user_id                    :integer
#  income_expenditure_type_id :integer
#  mount                      :float
#  description                :text
#  created_at                 :datetime         not null
#  updated_at                 :datetime         not null
#

class IncomeExpenditure < ApplicationRecord
  # associations
  belongs_to :user
  belongs_to :income_expenditure_type

  # validations
  validates :mount, numericality: { greater_than: 0 }

  #callbacks
  before_create :generate_uid

  private
    def generate_uid
      self.uid = SecureRandom.uuid
    end
end
