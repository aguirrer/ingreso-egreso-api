# == Schema Information
#
# Table name: income_expenditure_types
#
#  id          :integer          not null, primary key
#  name        :string
#  description :text
#  created_at  :datetime         not null
#  updated_at  :datetime         not null
#

class IncomeExpenditureType < ApplicationRecord
end
