# == Schema Information
#
# Table name: users
#
#  id              :integer          not null, primary key
#  uid             :string
#  name            :string
#  last_name       :string
#  username        :string
#  email           :string
#  password_digest :string
#  created_at      :datetime         not null
#  updated_at      :datetime         not null
#

class User < ApplicationRecord
  #associations
  has_secure_password
  has_many :income_expenditures, dependent: :destroy
  
  #validations
  VALID_EMAIL_REGEX = /\A[\w+\-.]+@[a-z\d\-]+(\.[a-z\d\-]+)*\.[a-z]+\z/i
  validates :email, presence: true, uniqueness: { case_sensitive: false }
  validates :email, format: { with: VALID_EMAIL_REGEX  }
  validates :password, length: { minimum: 6 }, if: -> { password.present? }
  validates :username, presence: true, uniqueness: { case_sensitive: false }
  
  #callbacks
  before_create :generate_uid
  before_save :downcase_email

  private
    def generate_uid
      self.uid = SecureRandom.uuid
    end

    def downcase_email
      self.email = self.email.downcase.delete(" ")
      self.username = self.username.downcase.delete(" ")
    end
end
