class Api::V1::UsersController < ApplicationController
  skip_before_action :authenticate_request!, only: [:create]

  def create
    user = User.new(user_params)
    if user.save
      render json: { message: 'User created successfully', code: 'successful_registration'}, status: :ok
    else
      render json: { errors: user.errors, code: 'registration_failed' }, status: :unprocessable_entity
    end
  end

  def show
    render json: { user: filter(@current_user), code: 'get_user_successfully' }, status: :ok
  end

  private
    def user_params
      params.require(:user)
      .permit(
        :name,
        :last_name,
        :username,
        :email,
        :password,
        :password_confirmation
      )
    end

    def filter(user)
      user_tmp = user
      user = user_tmp.attributes.except('id', 'password_digest')
      user['created_at'] = user_tmp.created_at.strftime("%d/%m/%Y %I:%M%p")
      user['updated_at'] = user_tmp.updated_at.strftime("%d/%m/%Y %I:%M%p")
      user
    end
end