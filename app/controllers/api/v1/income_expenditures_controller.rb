class Api::V1::IncomeExpendituresController < ApplicationController
  before_action :set_income_expenditure, only: [:destroy]

  def create
    @income_expenditure = @current_user.income_expenditures.build(income_expenditures_params) 
    if @income_expenditure.save
      render json: { message: 'IncomeExpenditure created successfully', code: 'income_expenditure_created_successfully' }, status: :ok
    else
      render json: { errors: @income_expenditure.errors, code: 'income_expenditure_created_failed' }, status: :unprocessable_entity
    end
  end

  def index
    @income_expenditures = @current_user.income_expenditures.map { |income_expenditure| filter(income_expenditure) }
    render json: { income_expenditures: @income_expenditures,  code: 'get_all_income_expenditures_successfully' }, status: :ok
  end

  def destroy
    @income_expenditure.destroy  
    render json: { message: 'IncomeExpenditure deleted successfully',  code: 'income_expenditure_deleted_successfully' }, status: :ok
  end

  private
    def income_expenditures_params
      params.require(:income_expenditure)
      .permit(
        :mount,
        :income_expenditure_type_id,
        :description
      )
    end

    def filter(income_expenditure)
      income_expenditure_tmp = income_expenditure
      income_expenditure = income_expenditure_tmp.attributes.except('user_id')
      income_expenditure['created_at'] = income_expenditure_tmp.created_at.strftime("%d/%m/%Y %I:%M%p")
      income_expenditure['updated_at'] = income_expenditure_tmp.updated_at.strftime("%d/%m/%Y %I:%M%p")
      income_expenditure
    end

    def set_income_expenditure
      @income_expenditure = IncomeExpenditure.find(params[:id])
    end
end