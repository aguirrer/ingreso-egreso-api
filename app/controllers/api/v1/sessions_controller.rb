class Api::V1::SessionsController < ApplicationController
  skip_before_action :authenticate_request!, only: [:login]

  def login
    login_key = params[:session][:email].downcase

    user = User.find_by(email: login_key)
    user = User.find_by(username: login_key) unless user
     
    if user && user.authenticate(params[:session][:password])
      auth_token = JsonWebToken.encode({uid: user.uid})
      render json: { user: filter(user), auth_token: auth_token, expires_at: Time.now + 1.day }, status: :ok
    else
      render json: { message: 'Username/Password invalid', code: 'invalid_login' }, status: :bad_request
    end
  end

  private
    def filter(user)
      user_tmp = user
      user = user_tmp.attributes.except('id', 'password_digest')
      user['created_at'] = user_tmp.created_at.strftime("%d/%m/%Y %I:%M%p")
      user['updated_at'] = user_tmp.updated_at.strftime("%d/%m/%Y %I:%M%p")
      user
    end
end