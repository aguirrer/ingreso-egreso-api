class ApplicationController < ActionController::API
  include Authenticable
  before_action :authenticate_request!

  protected
    def authenticate_request!
      if !payload || !JsonWebToken.valid_payload(payload.first)
        return invalid_authentication
      end

      load_current_user!
      invalid_authentication unless @current_user
    end

    def invalid_authentication
      render json: { message: 'Invalid request', code: 'invalid_request' }, status: :unauthorized
    end

  private
    def payload
      auth_header = request.headers['Authorization']
      token = auth_header.split(' ').last
      JsonWebToken.decode(token)
    end

    def load_current_user!
      @current_user = User.find_by(uid: payload[0]["uid"])
    end
end
